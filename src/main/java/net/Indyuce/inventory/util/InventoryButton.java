package net.Indyuce.inventory.util;

import io.lumine.mythic.lib.UtilityMethods;
import io.lumine.mythic.lib.util.AdventureUtils;
import net.Indyuce.inventory.MMOInventory;
import org.apache.commons.lang.Validate;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

public class InventoryButton {
	private final ItemStack item;

	public static final NamespacedKey NAMESPACED_KEY = new NamespacedKey(MMOInventory.plugin, "Button");

	/**
	 * Used to read an item icon from the config file. It can be a textured
	 * player head using the 'texture' config key, a modeled item using 'model'.
	 * Using 'material' you can choose the item material.
	 *
	 * @param config
	 *            Config to read from
	 */
	public InventoryButton(ConfigurationSection config) {
		Validate.notNull(config, "Config cannot be null");

		Validate.isTrue(config.contains("material"), "Could not find item material");
		this.item = new ItemStack(Material.valueOf(config.getString("material")));

		ItemMeta meta = item.getItemMeta();

		// Display name
		if (config.contains("name"))
			AdventureUtils.setDisplayName(meta, config.getString("name"));

		// Lore
		if (config.contains("lore"))
			AdventureUtils.setLore(meta, config.getStringList("lore"));

		// Skull texture
		if (config.contains("skull-texture") && item.getType() == Material.PLAYER_HEAD)
			UtilityMethods.setTextureValue(meta, config.getString("skull-texture"));

		// Apply custom model data
		if (config.contains("custom-model-data"))
			meta.setCustomModelData(config.getInt("custom-model-data"));

		// Apply correct NBTtag
		meta.getPersistentDataContainer().set(NAMESPACED_KEY, PersistentDataType.BYTE, (byte) 1);

		item.setItemMeta(meta);
	}

	public ItemStack getItem() {
		return item;
	}
}
